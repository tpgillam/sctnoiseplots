class ReadingPoint(object):
    def __init__(self, readingTime, voltage, current):
        self.readingTime = readingTime
        self.voltage = voltage
        self.current = current

    @classmethod
    def fromString(cls, readingString):
        tokens = readingString.strip().split()
        dateString = ' '.join(tokens[0:2])
        from datetime import datetime
        readingTime = datetime.strptime(dateString, '%Y-%m-%d %H:%M:%S.%f')
        voltage = float(tokens[2])
        current = float(tokens[3])
        return cls(readingTime, voltage, current)

    def __repr__(self):
        return '{0}: {1} {2}'.format(self.readingTime, self.voltage, self.current)


class ReadingPointList(list):
    def __getitem__(self, key):
        from datetime import datetime, timedelta
        first = True
        minDifference = 0
        minItem = None
        if isinstance(key, datetime):
            for item in self:
                diff = abs(key - item.readingTime)
                if first:
                    first = False
                    minDifference = diff
                    minItem = item
                if diff < minDifference:
                    minDifference = diff
                    minItem = item
            return minItem
        return list.__getitem__(self, key)


def loadPointsFromFile(filename):
    points = ReadingPointList()
    f = open(filename)
    for line in f:
        points.append(ReadingPoint.fromString(line))
    f.close()
    return points

IVReadings = loadPointsFromFile('/Users/thomas/Documents/PhD/SCT Upgrade/threePointTestParser/camResultsAll/HVReadings')
