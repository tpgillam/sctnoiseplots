_filename = '/Users/thomas/Documents/PhD/SCT Upgrade/threePointTestParser/camVoltageCapacitanceData'

def capacitanceForVoltage(voltage):
    f = open(_filename)
    for line in f:
        tokens = line.strip().split()
        testVoltage = float(tokens[1])
        if voltage == testVoltage:
            return float(tokens[2])
    raise KeyError('Voltage not in lookup')
    f.close()
