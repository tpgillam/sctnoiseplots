class TemperatureReadingPoint(object):
    def __init__(self, readingTime, temp1, temp2, temp3, humidity):
        self.readingTime = readingTime
        self.temp1 = temp1
        self.temp2 = temp2
        self.temp3 = temp3
        self.humidity = humidity

    @classmethod
    def fromString(cls, readingString):
        tokens = readingString.strip().split()
        dateString = ' '.join(tokens[0:2])
        from datetime import datetime
        readingTime = datetime.strptime(dateString, '%Y-%m-%d %H:%M:%S.%f')
        temp1 = float(tokens[2])
        temp2 = float(tokens[3])
        temp3 = float(tokens[4])
        humidity = float(tokens[5])
        return cls(readingTime, temp1, temp2, temp3, humidity)

    def __repr__(self):
        return '{0}: {1} {2} {3} {4}'.format(self.readingTime, self.temp1, self.temp2, self.temp3, self.humidity)


def loadPointsFromFile(filename):
    from voltageParser import ReadingPointList
    points = ReadingPointList()
    f = open(filename)
    for line in f:
        points.append(TemperatureReadingPoint.fromString(line))
    f.close()
    return points

environmentalReadings = loadPointsFromFile('/Users/thomas/Documents/PhD/SCT Upgrade/threePointTestParser/camResultsAll/EnvironmentalReadings')

if __name__ == '__main__':
    for reading in environmentalReadings:
        print reading
