#!/usr/bin/env python

#dates = ['20130403', '20130404']
#dates = ['20130409']
#dates = ['20130419', '20130420']
#dates = ['20130424', '20130425']#, '20130426']
#dates= ['20130524']
#dates= ['20130531', '20130601', '20130602', '20130603']
#dates= ['20130526', '20130527', '20130528']

dates = ['20130712', '20130713']
#dates = ['20130713', '20130714', '20130715']

# Limit the runs that we use, within the set of files
majorRunNumbers = None
#majorRunNumbers = [350]
from datetime import datetime
#startTime = None
#endTime = None

startTime = datetime(2013,07,12, 17,00)
endTime = datetime(2013,07,13, 04,10)

#startTime = datetime(2013,07,13, 10,20)
#endTime = datetime(2013,07,15, 18,10)

if len(dates) == 1:
    fileNameTag = dates[0]
else:
    fileNameTag = '-'.join([dates[0], dates[-1]])

if majorRunNumbers is not None:
    fileNameTag = '-'.join(dates)+'-'+'-'.join(('Run'+str(x) for x in majorRunNumbers))

def main():
    #hybridList = ['15_57', '15_58', '17_55', '17_56']
    #hybridList = ['8_0', '8_1']
    hybridList = ['9_0', '9_1']


    from odict import odict
    filesForHybrids = odict()
    baseDir = '/Users/thomas/Documents/PhD/SCT Upgrade/threePointTestParser/camResultsAll/'
    for hybrid in hybridList:
        import os.path
        filesForHybrids[hybrid] = []
        for date in dates:
            #filesForHybrids[hybrid].append(os.path.join(baseDir, 'ABCNmoduleM'+hybrid+'_'+date+'.txt'))
            #filesForHybrids[hybrid].append(os.path.join(baseDir, 'SP_Module_Cambridge_'+hybrid+'_'+date+'.txt'))
            filesForHybrids[hybrid].append(os.path.join(baseDir, 'DC_Module_Cambridge_'+hybrid+'_'+date+'.txt'))

    for hybrid in filesForHybrids:
        makePlots(hybrid, filesForHybrids[hybrid], majorRunNumbers, startTime, endTime)

def makePlots(hybrid, files, majorRunNumbers=None, startTime=None, endTime=None):
    channels = getChannelsInFiles(files, majorRunNumbers, startTime, endTime)
    #for key in channels:
    #    print key, '\n', channels[key]

    import matplotlib.pyplot as plt
    import matplotlib.dates as mdates
    figNormal = plt.figure(figsize=(12, 9))
    figNormalSansUncert = plt.figure(figsize=(12, 9))
    figAdjusted = plt.figure(figsize=(12, 9))
    figAdjustedSansUncert = plt.figure(figsize=(12, 9))

    red = 0
    blue = 1
    keys = []
    offsets = []
    offsetUncs = []
    adjustedTotal = []
    adjustedTotalSquared = []
    for key in channels:
        keys.append(key)
        times = []
        values = []
        uncs = []
        for dataPoint in channels[key]:
            times.append(dataPoint.time)
            values.append(dataPoint.innse)
            uncs.append(dataPoint.innseUncert)

        offset = sum(values)/len(values)
        from math import sqrt
        offsetUnc = sqrt(sum([x**2 for x in uncs]))/float(len(uncs))
        offsets.append(offset)
        offsetUncs.append(offsetUnc)
        xvals = mdates.date2num(times)
        dateFormatter = mdates.DateFormatter('%d %b %H:%M')
        up = [x+y for x,y in zip(values, uncs)]
        down = [x-y for x,y in zip(values, uncs)]

        adjusted = [x-offset for x in values]
        if len(offsets) == 1:
            for x in adjusted:
                adjustedTotal.append(x)
                adjustedTotalSquared.append(x**2)
        else:
            i = 0
            for x in adjusted:
                adjustedTotal[i] += x
                adjustedTotalSquared[i] += x**2
                i += 1

        adjustedUncs = []
        for i in range(len(values)):
            uncSum = 0.0 
            for j in range(len(values)):
                if j == i:
                    continue
                uncSum += (1.0/float(len(values)))**2 * uncs[j]**2
            adjustedUncs.append( sqrt((float((len(values)-1))/float(len(values)))**2 * uncs[i]**2    + uncSum) )
        adjustedUp = [x+y for x,y in zip(adjusted, adjustedUncs)]
        adjustedDown = [x-y for x,y in zip(adjusted, adjustedUncs)]

        color = (red, 0, blue, 1.0)
        fillcolor = (red, 0, blue, 0.2)

        plt.figure(figNormal.number)
        plt.title('Hybrid '+hybrid+', noise')
        ax = figNormal.add_subplot(111)
        plt.fill_between(xvals, down, up, color=fillcolor)
        plt.plot_date(xvals, values, '-', color=color)
        ax.set_xlabel('Time')
        ax.xaxis.set_major_formatter(dateFormatter)
        ax.set_ylabel('Noise')

        plt.figure(figNormalSansUncert.number)
        plt.title('Hybrid '+hybrid+', noise')
        ax = figNormalSansUncert.add_subplot(111)
        plt.plot_date(xvals, values, '-', color=color)
        ax.set_xlabel('Time')
        ax.xaxis.set_major_formatter(dateFormatter)
        ax.set_ylabel('Noise')
        #ax.set_ylim(540, 700)

        plt.figure(figAdjusted.number)
        plt.title('Hybrid '+hybrid+', adjusted noise')
        ax = figAdjusted.add_subplot(111)
        plt.fill_between(xvals, adjustedDown, adjustedUp, color=fillcolor)
        plt.plot_date(xvals, adjusted, '-', color=color)
        ax.set_xlabel('Time')
        ax.xaxis.set_major_formatter(dateFormatter)
        ax.set_ylabel('Noise $-$ mean')

        plt.figure(figAdjustedSansUncert.number)
        plt.title('Hybrid '+hybrid+', adjusted noise')
        ax = figAdjustedSansUncert.add_subplot(111)
        plt.plot_date(xvals, adjusted, '-', color=color)
        ax.set_xlabel('Time')
        ax.xaxis.set_major_formatter(dateFormatter)
        ax.set_ylabel('Noise $-$ mean')

        red += 0.05
        blue -= 0.05

    meanOffset = sum(offsets)/len(offsets)

    figNormal.autofmt_xdate()
    figNormalSansUncert.autofmt_xdate()
    figAdjusted.autofmt_xdate()
    figAdjustedSansUncert.autofmt_xdate()

    adjustedMean = [meanOffset + x/len(adjustedTotal) for x in adjustedTotal]
    #adjustedTotalUncs = [sqrt(y/len(adjustedTotal) - x**2) for x,y in zip(adjustedMean, adjustedTotalSquared)]
    #adjustedTotalUp = [x+y for x,y in zip(adjustedMean, adjustedTotalUncs)]
    #adjustedTotalDown = [x-y for x,y in zip(adjustedMean, adjustedTotalUncs)]

    figAdjustedMean = plt.figure(figsize=(12, 9))
    plt.title('Hybrid '+hybrid+', mean adjusted hybrid noise')
    ax = figAdjustedMean.add_subplot(111)
    #plt.fill_between(xvals, adjustedTotalDown, adjustedTotalUp, color=(0,0,1,0.2))
    plt.plot_date(xvals, adjustedMean, '-', color=(0,0,1))
    ax.set_xlabel('Time')
    ax.xaxis.set_major_formatter(dateFormatter)
    ax.set_ylabel('Noise $-$ mean')
    figAdjustedMean.autofmt_xdate()

    figAdjustedMeanVsVoltage = plt.figure(figsize=(12, 9))
    plt.title('Hybrid '+hybrid+', mean adjusted hybrid noise')
    ax = figAdjustedMeanVsVoltage.add_subplot(111)
    from voltageParser import IVReadings
    voltagexvals = [IVReadings[mdates.num2date(x).replace(tzinfo=None)].voltage for x in xvals]
    #plt.fill_between(voltagexvals, adjustedTotalDown, adjustedTotalUp, color=(0,0,1,0.2))
    plt.plot(voltagexvals, adjustedMean, '-', color=(0,0,1))
    plt.plot(voltagexvals[0], adjustedMean[0], 'o', markersize=10, color=(1,0,0))
    plt.plot(voltagexvals[-1], adjustedMean[-1], 'x', markersize=10, color=(1,0,0))
    ax.set_xlabel('Voltage (V)')
    ax.set_ylabel('Noise $-$ mean')

    figAdjustedMeanVsCapacitance = plt.figure(figsize=(12, 9))
    plt.title('Hybrid '+hybrid+', mean adjusted hybrid noise')
    ax = figAdjustedMeanVsCapacitance.add_subplot(111)
    from capacitance import capacitanceForVoltage
    capxvals = [capacitanceForVoltage(x) for x in voltagexvals]
    #plt.fill_between(capxvals, adjustedTotalDown, adjustedTotalUp, color=(0,0,1,0.2))
    plt.plot(capxvals, adjustedMean, '-', color=(0,0,1))
    plt.plot(capxvals[0], adjustedMean[0], 'o', markersize=10, color=(1,0,0))
    plt.plot(capxvals[-1], adjustedMean[-1], 'x', markersize=10, color=(1,0,0))
    ax.set_xlabel('Capacitance (pF)')
    ax.set_ylabel('Noise $-$ mean')

    # Environmental plots
    figVoltage = plt.figure()
    plt.title('Bias voltage')
    ax = figVoltage.add_subplot(111)
    plt.plot_date(xvals, voltagexvals, '-', color=(0,0,1))
    ax.set_xlabel('Time')
    ax.xaxis.set_major_formatter(dateFormatter)
    ax.set_ylabel('Bias voltage (V)')
    figVoltage.autofmt_xdate()

    figCurrent = plt.figure()
    plt.title('Current')
    ax = figCurrent.add_subplot(111)
    currentyvals = [IVReadings[mdates.num2date(x).replace(tzinfo=None)].current for x in xvals]
    plt.plot_date(xvals, currentyvals, '-', color=(0,0,1))
    ax.set_xlabel('Time')
    ax.xaxis.set_major_formatter(dateFormatter)
    ax.set_ylabel('Current (A)')
    figCurrent.autofmt_xdate()

    figTemperature = plt.figure()
    plt.title('Temperature')
    ax = figTemperature.add_subplot(111)
    from temperatureParser import environmentalReadings 
    temp1 = [environmentalReadings[mdates.num2date(x).replace(tzinfo=None)].temp1 for x in xvals]
    temp2 = [environmentalReadings[mdates.num2date(x).replace(tzinfo=None)].temp2 for x in xvals]
    temp3 = [environmentalReadings[mdates.num2date(x).replace(tzinfo=None)].temp3 for x in xvals]
    plt.plot_date(xvals, temp1, '-', color=(0,0,1))
    plt.plot_date(xvals, temp2, '-', color=(0,1,0))
    plt.plot_date(xvals, temp3, '-', color=(1,0,0))
    ax.set_xlabel('Time')
    ax.xaxis.set_major_formatter(dateFormatter)
    ax.set_ylabel('Temperature (celsius)')
    figTemperature.autofmt_xdate()



    figOffset = plt.figure()
    plt.title('Hybrid '+hybrid+', computed offsets')
    ax = figOffset.add_subplot(111)
    indices = range(len(keys))
    if len(keys) == 0:
        print 'Len(keys) == 0 for hybrid {0}!!'.format(hybrid)
    else:
        ax.errorbar(indices, offsets, yerr=offsetUncs, fmt='o', ecolor='g')
    plt.xlim(-1, len(keys))
    ax.set_xticks([x for x in indices])
    ax.set_xticklabels(keys, rotation=45)
    ax.set_xlabel('Channel ID')
    ax.set_ylabel('Mean noise')
    
    #plt.show()
    prefix = 'outputCam/'+fileNameTag+'_'
    figNormal.savefig(prefix+hybrid+'_normal'+'.pdf', format='pdf')
    figNormalSansUncert.savefig(prefix+hybrid+'_normalSansUncert'+'.pdf', format='pdf')
    figAdjusted.savefig(prefix+hybrid+'_adjusted'+'.pdf', format='pdf')
    figAdjustedSansUncert.savefig(prefix+hybrid+'_adjustedSansUncert'+'.pdf', format='pdf')
    figAdjustedMean.savefig(prefix+hybrid+'_adjustedMean'+'.pdf', format='pdf')
    figAdjustedMeanVsVoltage.savefig(prefix+hybrid+'_adjustedMeanVsVoltage'+'.pdf', format='pdf')
    figAdjustedMeanVsCapacitance.savefig(prefix+hybrid+'_adjustedMeanVsCapacitance'+'.pdf', format='pdf')
    figOffset.savefig(prefix+hybrid+'_offset'+'.pdf', format='pdf')
    figVoltage.savefig(prefix+hybrid+'_voltage'+'.pdf', format='pdf')
    figCurrent.savefig(prefix+hybrid+'_current'+'.pdf', format='pdf')
    figTemperature.savefig(prefix+hybrid+'_temperature'+'.pdf', format='pdf')

    printCSV(prefix+hybrid+'_adjustedMean.csv', voltagexvals, xvals, capxvals, adjustedMean)


def printCSV(filename, voltagexvals, timexvals, capxvals, adjustedMean):
    f = open(filename, 'w')
    f.write('Voltage,Time,Capacitance,Noise\n')
    for line in zip(voltagexvals, timexvals, capxvals, adjustedMean):
        f.write('{0},{1},{2},{3}\n'.format(line[0], line[1], line[2], line[3]))
    f.close()

def getChannelsInFiles(filenames, majorRunNumbers=None, startTime=None, endTime=None):
    mergedChannels = None
    for filename in filenames:
        channels = getChannelsInFile(filename, majorRunNumbers, startTime, endTime)
        if mergedChannels is None:
            mergedChannels = channels
        else:
            for key in mergedChannels:
                for dataPoint in channels[key]:
                    mergedChannels[key].addDataPoint(dataPoint)
    return mergedChannels

def getChannelsInFile(filename, majorRunNumbers=None, startTime=None, endTime=None):
    from odict import odict
    channels = odict()

    from parser import threePointGainChunksInFile
    first = True
    for chunker in threePointGainChunksInFile(filename):
        if majorRunNumbers is not None and chunker.header.runNumberMajor() not in majorRunNumbers:
            continue

        from datetime import datetime
        runTime = datetime.combine(chunker.header.date, chunker.daqInfo.time)

        if startTime is not None and runTime < startTime:
            continue
        if endTime is not None and runTime > endTime:
            continue

        if first:
            first = False
            for reading in chunker.threePointGain.readingList:
                channels[reading.label] = ChannelData()

        capacitance = chunker.threePointGain.capacitance
        for reading in chunker.threePointGain.readingList:
            #channels[reading.label].createDataPoint(runTime, reading.gain, reading.gainUncert)
            channels[reading.label].createDataPoint(runTime, reading.innse, reading.innseUncert)
    return channels

class ChannelData(object):
    class DataPoint(object):
        def __init__(self, time, innse, innseUncert):
            self.time = time
            self.innse = innse
            self.innseUncert = innseUncert

    def __init__(self):
        self.dataPoints = []

    def createDataPoint(self, time, innse, innseUncert):
        self.dataPoints.append(ChannelData.DataPoint(time, innse, innseUncert))

    def addDataPoint(self, dataPoint):
        self.dataPoints.append(dataPoint)

    def __str__(self):
        result = ''
        for dataPoint in self.dataPoints:
            result += '{0}: {1} ({2})\n'.format(dataPoint.time, dataPoint.innse, dataPoint.innseUncert)
        return result
 
    def __iter__(self):
        for dataPoint in self.dataPoints:
            yield dataPoint


if __name__ == '__main__':
    main()
