#!/usr/bin/env python

def main():
    #testPath = '/Users/thomas/Documents/PhD/threePointTestParser/examples/CambsModule3_61_20120921.txt'
    #testPath = '/Users/thomas/Documents/PhD/threePointTestParser/examples/results/ABCNmodule0_61_20120711.txt'
    testPath = '/Users/thomas/Documents/PhD/SCT Upgrade/threePointTestParser/Results 2012-10-10/ABCNmoduleM15_57_20121009.txt'

    for test in testsInFile(testPath):
        chunker = TestChunker(test)
        if chunker.testID == 'ThreePointGain':
            #print chunker

            from datetime import datetime
            print datetime.combine(chunker.header.date, chunker.daqInfo.time)
            print chunker.threePointGain.capacitance
            for reading in chunker.threePointGain.readingList:
                print reading.label, ':', reading.innse, reading.innseUncert

def threePointGainChunksInFile(filename):
    for test in testsInFile(filename):
        chunker = TestChunker(test)
        if chunker.testID == 'ThreePointGain':
            yield chunker

class TestChunker(object):
    def __init__(self, testLines):
        self.header = None
        self.daqInfo = None
        self.dcsInfo = None
        self.scanInfo = None
        self.testID = None
        self._testLines = testLines
        self._currIndex = 0
        self._nonTestBlockNames = set(['DAQ_INFO', 'DCS_INFO', 'SCAN_INFO', 'Comment', 'Defect', 'TEST Rawdata'])
        self._parse()

    def _parse(self):
        self._getHeader()
        while self._atBlockStart():
            self._fillBlock()

    def _atBlockStart(self):
        if self._currIndex > len(self._testLines)-1:
            return False
        return self._testLines[self._currIndex].startswith('%')

    def _nextBlockName(self):
        if not self._atBlockStart():
            raise Exception('Need to be at start of block!')
        return self._testLines[self._currIndex][1:]

    def _fillBlock(self):
        blockName = self._nextBlockName()
        if blockName == 'DAQ_INFO':
            self._getDAQInfo()
        elif blockName == 'DCS_INFO':
            self._getDCSInfo()
        elif blockName == 'SCAN_INFO':
            self._getScanInfo()
        elif blockName in self._nonTestBlockNames:
            self._gobbleBlock()
        elif blockName not in self._nonTestBlockNames:
            self._getTestID()
            if self.testID == 'ThreePointGain':
                #print 'Parsing three point gain...'
                self._getThreePointGain()
            else:
                self._gobbleBlock()
        else:
            print 'Unknown block: '+self._nextBlockName()
            raise Exception('Unknown block!')

    def _getHeader(self):
        self.header = HeaderBlock()
        for line in self._blockMuncher():
            label = line.split(':')[0].strip()
            value = line.split(':')[1].strip()
            self.header[label] = value
    
    def _getDAQInfo(self):
        self.daqInfo = DAQInfoBlock()
        label = ''
        value = ''
        for line in self._blockMuncher():
            if line.startswith('#'):
                label = line[1:]
            else:
                value = line[1:-1]
                self.daqInfo[label] = value

    def _getDCSInfo(self):
        self.dcsInfo = DCSInfoBlock()
        label = ''
        value = ''
        for line in self._blockMuncher():
            if line.startswith('#'):
                label = line[1:]
            else:
                value = line
                if len(label.split()) > 1:
                    labels = [x.strip() for x in label.split()]
                    values = [x.strip() for x in value.split()]
                    label0 = labels[0]; label1 = labels[1];
                    value0 = values[0]; value1 = values[1];
                    self.dcsInfo[label0] = value0
                    self.dcsInfo[label1] = value1
                else:
                    self.dcsInfo[label] = value

    def _getScanInfo(self):
        self.scanInfo = ScanInfoBlock()
        label = ''
        value = ''
        for line in self._blockMuncher():
            line = line.strip('. \t')
            if line == '':
                continue
            if line.startswith('#'):
                label = line[1:]
            else:
                if line.startswith('"'):
                    value = line[1:-1]
                else:
                    value = line
                self.scanInfo[label] = value

    def _getTestID(self):
        candidateName = self._nextBlockName()
        if candidateName in self._nonTestBlockNames:
            print 'candidateName: '+candidateName
            raise Exception('Next block is not a test!')
        self.testID = candidateName

    def _getThreePointGain(self):
        self.threePointGain = ThreePointGain()
        inInfoSection = False
        label = None
        params = None
        for line in self._blockMuncher():
            if 'Loop B' in line:
                self.threePointGain.capacitance = float(line.split()[-1].strip()[:-2])
            if not inInfoSection and 'vt50' in line:
                inInfoSection = True
                continue
            if inInfoSection and 'Loop C' in line:
                inInfoSection = False
                continue
            if not inInfoSection:
                continue
            if line.startswith('#'):
                label = line.strip()[1:]
            else:
                params = line.strip().split()
                self.threePointGain.addReading(label, params)

    def _gobbleBlock(self):
        for line in self._blockMuncher():
            pass

    def _blockMuncher(self):
        first = True
        for line in self._testLines[self._currIndex:]:
            if first:
                first = False
                if self._atBlockStart():
                    self._currIndex += 1
                    continue
            if line.startswith('%'):
                break
            yield line
            self._currIndex += 1

    def __str__(self):
        result = 'BEGIN NEW TEST!!!!\n\n'
        result += 'Test ID: '+str(self.testID)+'\n\n'
        result += str(self.header)+'\n'
        result += str(self.daqInfo)+'\n'
        result += str(self.dcsInfo)+'\n'
        if self.scanInfo is not None:
            result += str(self.scanInfo)+'\n'
        #result += '\nUNPROCESSED LINES --\n'
        #for line in self._testLines:
        #    result += line+'\n'
        #result += '\n\n'
        return result


class BlockStorage(object):
    def __init__(self):
        self._setAttributeMap()
        self._storage = {}

    def __setitem__(self, label, value):
        name = self._attrMap[label]
        self._storage[name] = self._processedInput(label, value)

    def __getattr__(self, name):
        return self._storage[name]

    def __str__(self):
        result = self._name()+' --\n'
        for key in self._storage:
            result += '{0}: {1}'.format(key, self._storage[key])+'\n'
        return result

    def _setAttributeMap(self):
        raise NotImplementedError

    def _processedInput(self, label, value):
        raise NotImplementedError

    def _name(self):
        raise NotImplementedError


class HeaderBlock(BlockStorage):
    def _setAttributeMap(self):
        self._attrMap = {
                'SERIAL NUMBER' : 'serial',
                'TEST MADE BY'    : 'author',
                'LOCATION NAME' : 'location',
                'Run number'        : 'runNumber',
                'TEST_DATE'         : 'date',
                'PASSED'                : 'passed',
                'PROBLEM'             : 'problem'
            }

    def _processedInput(self, label, value):
        if label == 'PASSED' or label == 'PROBLEM':
            if value == 'YES': return True
            elif value == 'NO': return False
            else:
                print 'Unknown boolean equivalent: '+value
                raise Exception('Unable to process value')
        elif label == 'TEST_DATE':
            from datetime import date
            day = int(value.split('/')[0])
            month = int(value.split('/')[1])
            year = int(value.split('/')[2])
            return date(year, month, day)
        return value

    def _name(self):
        return 'HEADER INFORMATION'

    def runNumberMajor(self):
        return int(self.runNumber.split('-')[0])


class DAQInfoBlock(BlockStorage):
    def _setAttributeMap(self):
        self._attrMap = {
                'HOST'        : 'host',
                'VERSION' : 'version',
                'DUT'         : 'dut',
                'TIME'        : 'time'
            }

    def _processedInput(self, label, value):
        if label == 'TIME':
            from datetime import time
            hour = int(value.split(':')[0])
            minute = int(value.split(':')[1])
            second = int(value.split(':')[2])
            return time(hour, minute, second)
        return value

    def _name(self):
        return 'DAQ INFORMATION'


class DCSInfoBlock(BlockStorage):
    def _setAttributeMap(self):
        self._attrMap = {
                'T0'                     : 't0',
                'T1'                     : 't1',
                'VDET'                 : 'vdet',
                'IDET'                 : 'idet',
                'VCC'                    : 'vcc',
                'ICC'                    : 'icc',
                'VDD'                    : 'vdd',
                'IDD'                    : 'idd',
                'TIME_POWERED' : 'timePowered'
            }

    def _processedInput(self, label, value):
        if not label == 'TIME_POWERED':
            return float(value)
        elif label == 'TIME_POWERED':
            if value == '.':
                return None
            else: 
                return value
        return value

    def _name(self):
        return 'DCS INFORMATION'


class ScanInfoBlock(BlockStorage):
    def _setAttributeMap(self):
        self._attrMap = {
                'POINT_TYPE' : 'pointType',
                'N_POINTS'     : 'nPoints',
                'POINTS'         : 'points'
            }

    def _processedInput(self, label, value):
        if label == 'N_POINTS':
            return int(value)
        elif label == 'POINTS':
            return [float(x.strip()) for x in value.split()]
        return value

    def _name(self):
        return 'SCAN INFORMATION'


class ThreePointGain(object):
    class Reading(object):
        def __init__(self, label, params):
            self.label = label
            self.vt50 = float(params[0])
            self.vt50Uncert = float(params[1])
            self.gain = float(params[2])
            self.gainUncert = float(params[3])
            self.offset = float(params[4])
            self.offsetUncert = float(params[5])
            self.outnse = float(params[6])
            self.innse = float(params[7])
            self.innseUncert = float(params[8])

        def __str__(self):
            return '{0}: {1} {2}'.format(self.label, self.innse, self.innseUncert)

    def __init__(self):
        self.capacitance = 1.0
        self.readingList = []

    def addReading(self, label, params):
        self.readingList.append(ThreePointGain.Reading(label, params))

    def __str__(self):
        result = ''
        for reading in self.readingList:
            result += str(reading)+'\n'
        return result


def testsInFile(filePath):
    f = open(filePath)
    testLines = None
    for line in interestingLines(f):
        if line == '%NewTest':
            if testLines is not None:
                yield testLines
            testLines = []
            continue
        if testLines is None:
            testLines = []
        testLines.append(line)
    if len(testLines) > 0:
        yield testLines
    f.close()

def interestingLines(fileObject):
    for line in fileObject:
        line = line.strip()
        if line == '' or line == '#':
            continue
        yield line

if __name__ == '__main__':
    main()
