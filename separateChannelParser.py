#!/usr/bin/env python

comments = set()
dates = ['20130403', '20130404']
fileNameTag = '-'.join(dates) 

def main():
    #hybridList = ['15_57', '15_58', '17_55', '17_56']
    hybridList = ['8_0', '8_1']
    baseDir = '/Users/thomas/Documents/PhD/SCT Upgrade/threePointTestParser/camResultsAll/'

    from odict import odict
    filesForHybrids = odict()
    for hybrid in hybridList:
        filesForHybrids[hybrid] = []
        for date in dates:
            filesForHybrids[hybrid].append('DC_Module_Cambridge_'+hybrid+'_'+date+'.txt')

    for hybrid in filesForHybrids:
        infiles = filesForHybrids[hybrid]
        print infiles
        import os.path
        from datetime import datetime
        #starttime = datetime(2012,10,10,10, 45)
        #endtime = datetime(2012,10,10,11))
        starttime = None
        #starttime = datetime(2012,10,26, 17,00)
        #starttime = datetime(2012,10,27, 00,00)
        #starttime = datetime(2012,10,30, 10,10)
        #starttime = datetime(2012,10,30, 11,45)
        endtime = None
        noiseStitcher = ChannelResultsStitcher(baseDir, infiles, 'innse', starttime=starttime, endtime=endtime)
        gainStitcher = ChannelResultsStitcher(baseDir, infiles, 'gain', starttime=starttime, endtime=endtime)
        offsetStitcher = ChannelResultsStitcher(baseDir, infiles, 'offset', starttime=starttime, endtime=endtime)

        class Package(object):
            pass
        package = Package()
        package.noiseMeans, package.noiseSigmas = getMeansSigmasForEachChannel(noiseStitcher.dataSeries)
        package.gainMeans, package.gainSigmas = getMeansSigmasForEachChannel(gainStitcher.dataSeries)
        package.offsetMeans, package.offsetSigmas = getMeansSigmasForEachChannel(offsetStitcher.dataSeries)
        #offsetMeans, offsetSigmas = getMeanoffsetsSigmasForEachChannel(noiseStitcher.dataSeries)

        figure = makeChannelStatsPlotColoured(package)
        prefix = 'outputCam/'+fileNameTag+'_'
        figure.savefig(prefix+'perchannel_'+hybrid+'.pdf', format='pdf')

        if hybrid == '15_57':
            zoomFigure = makeZoomedGainPlot(package)
            zoomFigure.savefig(prefix+'perchannelZoomGain_'+hybrid+'.pdf', format='pdf')
            zoomFigureColoured = makeZoomedGainPlotColoured(package)
            zoomFigureColoured.savefig(prefix+'perchannelZoomGainColoured_'+hybrid+'.pdf', format='pdf')
        #import matplotlib.pyplot as plt
        #plt.show()

    print comments


def makeChannelStatsPlot(package):
    import matplotlib.pyplot as plt
    numChannels = len(package.noiseMeans)

    figChannelStats = plt.figure(figsize=(24, 12))
    #plt.title('Channel noise')
    ax1 = figChannelStats.add_subplot(311)
    ax2 = figChannelStats.add_subplot(312)
    ax3 = figChannelStats.add_subplot(313)
    indices = getIndices(numChannels)
    ax1.errorbar(indices, package.noiseMeans, yerr=package.noiseSigmas, fmt='o', ecolor='g')
    ax2.errorbar(indices, package.gainMeans, yerr=package.gainSigmas, fmt='o', ecolor='g')
    ax3.errorbar(indices, package.offsetMeans, yerr=package.offsetSigmas, fmt='o', ecolor='g')
    #ax1.set_xlabel('Channel')
    ax1.xaxis.set_visible(False)
    ax1.set_ylabel('Noise')
    ax1.set_xlim(-1, numChannels)
    ax1.set_ylim(400, 800)
    #ax2.set_xlabel('Channel')
    ax2.xaxis.set_visible(False)
    ax2.set_ylabel('Gain')
    ax2.set_xlim(-1, numChannels)
    ax2.set_ylim(100, 115)
    ax3.set_xlabel('Channel')
    ax3.set_ylabel('Offset')
    ax3.set_xlim(-1, numChannels)
    ax3.set_ylim(-10, 50)
    plotLinesAtChannelBoundaries(ax1)
    plotLinesAtChannelBoundaries(ax2)
    plotLinesAtChannelBoundaries(ax3)

    return figChannelStats

def makeChannelStatsPlotColoured(package):
    import matplotlib.pyplot as plt
    numChannels = len(package.noiseMeans)

    indices = getIndices(numChannels)
    indices1 = getIndices(numChannels)[::2]
    noise1 = package.noiseMeans[::2]
    indices2 = getIndices(numChannels)[1::2]
    noise2 = package.noiseMeans[1::2]

    indices01 = getIndices(numChannels)[::4]
    gain1 = package.gainMeans[::4]
    indices02 = getIndices(numChannels)[1::4]
    gain2 = package.gainMeans[1::4]
    indices03 = getIndices(numChannels)[2::4]
    gain3 = package.gainMeans[2::4]
    indices04 = getIndices(numChannels)[3::4]
    gain4 = package.gainMeans[3::4]

    figChannelStats = plt.figure(figsize=(24, 12))
    #plt.title('Channel noise')
    ax1 = figChannelStats.add_subplot(311)
    ax2 = figChannelStats.add_subplot(312)
    ax3 = figChannelStats.add_subplot(313)
    ax1.plot(indices1, noise1, 'o', color='b')
    ax1.plot(indices2, noise2, 'o', color='y')
    #ax2.errorbar(indices, package.gainMeans, yerr=package.gainSigmas, fmt='o', ecolor='g')
    ax2.plot(indices01, gain1, 'o', color='r')
    ax2.plot(indices02, gain2, 'o', color='g')
    ax2.plot(indices03, gain3, 'o', color='b')
    ax2.plot(indices04, gain4, 'o', color='y')
    ax3.errorbar(indices, package.offsetMeans, yerr=package.offsetSigmas, fmt='o', ecolor='g')
    #ax1.set_xlabel('Channel')
    ax1.xaxis.set_visible(False)
    ax1.set_ylabel('Noise')
    ax1.set_xlim(-1, numChannels)
    ax1.set_ylim(400, 800)
    #ax2.set_xlabel('Channel')
    ax2.xaxis.set_visible(False)
    ax2.set_ylabel('Gain')
    ax2.set_xlim(-1, numChannels)
    ax2.set_ylim(100, 115)
    ax3.set_xlabel('Channel')
    ax3.set_ylabel('Offset')
    ax3.set_xlim(-1, numChannels)
    ax3.set_ylim(-10, 50)
    plotLinesAtChannelBoundaries(ax1)
    plotLinesAtChannelBoundaries(ax2)
    plotLinesAtChannelBoundaries(ax3)

    return figChannelStats

def makeZoomedGainPlot(package):
    import matplotlib.pyplot as plt
    numChannels = len(package.noiseMeans)

    zoomFigure = plt.figure(figsize=(24, 12))
    ax2 = zoomFigure.add_subplot(111)
    indices = getIndices(numChannels)
    ax2.errorbar(indices, package.gainMeans, yerr=package.gainSigmas, fmt='o-', ecolor='g')
    #ax2.plot(indices, package.gainMeans, 'o-')
    ax2.set_xlabel('Channel')
    ax2.set_ylabel('Gain')
    ax2.set_xlim(890, 1160)
    ax2.set_ylim(102, 112)
    plotLinesAtChannelBoundaries(ax2)

    return zoomFigure

def makeZoomedGainPlotColoured(package):
    import matplotlib.pyplot as plt
    numChannels = len(package.noiseMeans)

    indices1 = getIndices(numChannels)[::4]
    gains1 = package.gainMeans[::4]
    indices2 = getIndices(numChannels)[1::4]
    gains2 = package.gainMeans[1::4]
    indices3 = getIndices(numChannels)[2::4]
    gains3 = package.gainMeans[2::4]
    indices4 = getIndices(numChannels)[3::4]
    gains4 = package.gainMeans[3::4]

    zoomFigure = plt.figure(figsize=(10, 6))
    ax2 = zoomFigure.add_subplot(111)
    #ax2.errorbar(indices, package.gainMeans, yerr=package.gainSigmas, fmt='o-', ecolor='g')
    color1 = [x/255.0 for x in (187, 213, 240, 255)]
    color2 = [x/255.0 for x in (142, 190, 237, 255)]
    color3 = [x/255.0 for x in (9, 99, 189, 255)]
    color4 = [x/255.0 for x in (4, 54, 105, 255)]
    ax2.plot(indices1, gains1, 'or')#, color=color1)
    ax2.plot(indices2, gains2, 'og')#, color=color2)
    ax2.plot(indices3, gains3, 'ob')#, color=color3)
    ax2.plot(indices4, gains4, 'oy')#, color=color4)
    ax2.set_xlabel('Channel')
    ax2.set_ylabel('Gain')
    ax2.set_xlim(760, 1160)
    ax2.set_ylim(103, 111)
    plotLinesAtChannelBoundaries(ax2)

    return zoomFigure

def plotLinesAtChannelBoundaries(axis):
    for i in range(1,20):
        axis.axvline(128.0*i-0.5,0,500,color='r')
    axis.axvline(1280.5,0,500,linewidth=3,color='r')

    #figOffsetChannelNoise = plt.figure()
    #plt.title('Channel noise')
    #ax2 = figOffsetChannelNoise.add_subplot(111)
    #indices = range(numChannels)
    #ax2.errorbar(indices, offsetMeans, yerr=offsetSigmas, fmt='o', ecolor='g')
    #plt.xlim(-1, numChannels)
    #ax2.set_xlabel('Channel')
    #ax2.set_ylabel('Mean noise')

def getIndices(numChannels):
    indices = range(numChannels/2)
    for i in range(10):
        newIndices = [numChannels/2 + i*128 + x for x in range(128)]
        newIndices.reverse()
        indices += newIndices
    return indices

def getMeanoffsetsSigmasForEachChannel(dataSeries):
    timeMeans, timeSigmas = getMeanSigmasForEachTime(dataSeries)
    means = []
    sigmas = []

    for pointSet in dataSeries:
        count = 0
        sumx = 0.0
        sumx2 = 0.0
        timeIndex = 0
        for point in pointSet:
            count += 1
            sumx += point.value - timeMeans[timeIndex]
            sumx2 += (point.value - timeMeans[timeIndex])**2
            #print point.time, point.value
            timeIndex += 1
        mean = sumx/float(count)
        from math import sqrt
        sigma = sqrt(sumx2/float(count) - mean**2)
        means.append(mean)
        sigmas.append(sigma)
    return means, sigmas

def getMeanSigmasForEachTime(dataSeries):
    values = get2DValueArray(dataSeries)
    numChannels = len(values)
    numTimesteps = len(values[0])
    means = []
    sigmas = []
    for time in range(numTimesteps):
        sumx = 0.0
        sumx2 = 0.0
        for channel in range(numChannels):
            sumx += values[channel][time]
            sumx2 += values[channel][time]**2
        from math import sqrt
        mean = sumx/float(numChannels)
        sigma = sqrt(sumx2/float(numChannels) - mean**2)
        means.append(mean)
        sigmas.append(sigma)
    return means, sigmas

def get2DValueArray(dataSeries):
    # Indexed[channel][timestep]
    values = []
    for pointSet in dataSeries:
        values.append([])
        for point in pointSet:
            values[-1].append(point.value)
    return values

def getMeansSigmasForEachChannel(dataSeries):
    means = []
    sigmas = []

    for pointSet in dataSeries:
        count = 0
        sumx = 0.0
        sumx2 = 0.0
        for point in pointSet:
            count += 1
            sumx += point.value
            sumx2 += point.value**2
            #print point.time, point.value
        if count == 0:
            return means, sigmas
        mean = sumx/float(count)
        from math import sqrt
        sigma2 = sumx2/float(count) - mean**2
        if sigma2 < 0:
            sigma = 0
        else:
            sigma = sqrt(sumx2/float(count) - mean**2)
        means.append(mean)
        sigmas.append(sigma)
    return means, sigmas


class ChannelResultsStitcher(object):
    def __init__(self, resultsPath, filenames, element='innse', starttime=None, endtime=None):
        import os.path
        self._filenames = [os.path.join(resultsPath, x) for x in filenames]
        self._resultsPath = resultsPath
        self._element = element
        self._starttime = starttime
        self._endtime = endtime
        self._go()

    def _go(self):
        self.dataSeries = []
        first = True
        for runTime, channels in self._timeAndChannelData():
            if first:
                first = False
                for i in range(len(channels)):
                    self.dataSeries.append(DataSeries())
            for channel in channels:
                if self._starttime is not None:
                    if runTime < self._starttime:
                        continue
                if self._endtime is not None:
                    if runTime > self._endtime:
                        continue
                self.dataSeries[channel.index].append(DataPoint(runTime, getattr(channel, self._element)))

    def _timeAndChannelData(self):
        from parser import threePointGainChunksInFile
        for filename in self._filenames:
            for chunker in threePointGainChunksInFile(filename):
                from datetime import datetime
                runTime = datetime.combine(chunker.header.date, chunker.daqInfo.time)
                rcFileName = '{0}_RC_{1}.txt'.format(chunker.header.serial, chunker.header.runNumber.replace('-', '_'))
                import os.path
                rcFilePath = os.path.join(self._resultsPath, rcFileName)
                parser = SeparateChannelParser(rcFilePath)
                yield (runTime, parser.channels)


class DataSeries(object):
    def __init__(self):
        self._dataPoints = []

    def append(self, dataPoint):
        self._dataPoints.append(dataPoint)

    def __iter__(self):
        for dataPoint in self._dataPoints:
            yield dataPoint


class DataPoint(object):
    def __init__(self, time, value):
        self.time = time
        self.value = value


class SeparateChannelParser(object):
    def __init__(self, filename):
        self._filename = filename
        self._parseData()

    def _parseData(self):
        self.channels = []
        for channel in self._channelsInFile():
            self.channels.append(channel)
    
    def _channelsInFile(self):
        class Channel(object):
            def __init__(self, index, code, gain, offset, innse, comment):
                self.index = int(index)
                self.code = code
                self.gain = float(gain)
                self.offset = float(offset)
                self.innse = float(innse)
                self.comment = comment
                comments.add(comment)

            @classmethod
            def initFromTokens(Klass, tokens):
                tokens = [x.strip() for x in tokens]
                return Klass(tokens[0], tokens[1], tokens[2], tokens[3], tokens[4], tokens[5])

        for line in self._interestingLines():
            yield Channel.initFromTokens(line.split())

    def _interestingLines(self):
        f = open(self._filename)
        for line in f:
            line = line.strip()
            if line == '':
                continue
            if line.startswith('#'):
                continue
            yield line
        f.close()
 
if __name__ == '__main__':
    main()
